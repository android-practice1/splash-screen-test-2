## Splash Screen 

To Do ~ 

1. Create a new activity for splash screen

2. add a image on layout *no image id is required*

   > For higher quality and size image you;ll need to use **generalized densities/sizes or glide**

   ```xml
   <ImageView
              android:id="@+id/imageView"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:src="@drawable/background_image">
   </ImageView>
   ```

3. Change your startup activity on `AndroidMenifest` file to second activity. 

Create a thread and set time to sleep after that redirect to main app screen.

```java	
/****** Create Thread that will sleep for 5 seconds****/  		
Thread background = new Thread() {
    public void run() {
        try {
            // Thread will sleep for 5 seconds
            sleep(5*1000);

            // After 5 seconds redirect to another intent
            Intent i=new Intent(getBaseContext(),FirstScreen.class);
            startActivity(i);

            //Remove activity
            finish();
        } catch (Exception e) {
        }
    }
};

// start thread
background.start();
```

### or 

you can use this method in `onCreate` method 

EXAMPLE ~ https://gitlab.com/android-practice1/splash-screen-test-2

```java
new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
    // Using handler with postDelayed called runnable run method
    @Override
    public void run() {
      	Intent i = new Intent(MainSplashScreen.this, FirstScreen.class);
        startActivity(i);
        // close this activity
        finish();
    }
}, 5*1000); // wait for 5 seconds or you can use this as global veriable
```

### TESTED AND OK

Farhan Sadik 

